//KMP Algorithm for searching patterns directly in the response byte array.
// Adapted from: 
// Copyright (c) 2014 Project Nayuki
// https://www.nayuki.io/page/knuth-morris-pratt-string-matching

export class KMPAlgorithmImpl {

    //Compute KMP table.
    public static kmpTable(pattern: number[]): number[] {
        let lsp = [0];  // Base case
        for (let i = 1; i < pattern.length; i++) {
            let j = lsp[i - 1];  // Start by assuming we're extending the previous LSP
            while (j > 0 && pattern[i] != pattern[j])
                j = lsp[j - 1];
            if (pattern[i] == pattern[j])
                j++;
            lsp.push(j);
        }
        return lsp;
    }

    public static kmpSearchInSubArray(pattern: number[], text: Uint8Array, start: number, end: number, lsp?: number[]) {
        if (pattern.length == 0)
            return 0;  // Immediate match

        // Compute longest suffix-prefix table
        if (!lsp) {
            lsp = [0];  // Base case
            for (let i = 1; i < pattern.length; i++) {
                let j = lsp[i - 1];  // Start by assuming we're extending the previous LSP
                while (j > 0 && pattern[i] != pattern[j])
                    j = lsp[j - 1];
                if (pattern[i] == pattern[j])
                    j++;
                lsp.push(j);
            }
        }

        // Walk through text string
        let j: any = 0;  // Number of chars matched in pattern
        for (let i = start; i < end; i++) {
            while (j > 0 && text[i] != pattern[j])
                j = lsp[j - 1];  // Fall back in the pattern
            if (text[i] == pattern[j]) {
                j++;  // Next char matched, increment position
                if (j == pattern.length)
                    return i - (j - 1);
            }
        }
        return -1;  // Not found
    }

    //KMP searching function. 
    public static kmpSearch(pattern: number[], text: Uint8Array, lsp?: number[]): number {
        let start = 0;
        let end = text.length;

        return this.kmpSearchInSubArray(pattern, text, start, end, lsp);
    }

}