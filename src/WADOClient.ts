import { KMPAlgorithmImpl } from './KMPAlgorithmImpl';
import { TextEncoder } from 'text-encoding';

export interface ErrorCallbackType { (this: XMLHttpRequestEventTarget, ev: ErrorEvent): any }
export interface MyErrorCallback { (error_type: ERROR): any }
export interface SingleFrameSuccessCallback { (objectURL: string): void }
export interface MultiFrameSuccessCallback { (object_url_list: string[]): void }

export interface FrameRequester {
  requestFrame(study_instance_uid: string, series_instance_uid: string, sop_instance_uid: string, frame_number: number, callback: SingleFrameSuccessCallback): void;
  requestFrames(study_instance_uid: string, series_instance_uid: string, sop_instance_uid: string, frame_number: number[], callback: MultiFrameSuccessCallback): void;
}

export class WADOClient implements FrameRequester {

  private static START_CONTENT_PATTERN = { pattern: [13, 10, 13, 10], lsp: KMPAlgorithmImpl.kmpTable([13, 10, 13, 10]) };
  private static END_PATTERN = { pattern: [45, 45, 102, 114, 97, 109, 101, 45, 45], lsp: KMPAlgorithmImpl.kmpTable([45, 45, 102, 114, 97, 109, 101, 45, 45]) };

  private url: string;

  private encoder: any;

  private onerror_callback: MyErrorCallback;

  constructor(endpoint_url: string) {
    this.url = endpoint_url;
    if (this.url.charAt(this.url.length - 1) != '/')
      this.url = this.url + '/';
    this.encoder = new TextEncoder('utf-8');
  }

  set onerror(callback: MyErrorCallback) {
    this.onerror_callback = callback;
  }

  public requestFrame(study_instance_uid: string, series_instance_uid: string, sop_instance_uid: string, frame_number: number, callback: SingleFrameSuccessCallback) {
    let self = this;
    //let url = this.url + 'studies/0.0.0.0/series/1.0.1.0/instances/1.2.392.200140.2.1.1.1.4.1339200792.1816.1458823325.464/frames/0';
    var xhr = new XMLHttpRequest();
    xhr.open('GET', `${this.url}studies/${study_instance_uid}/series/${series_instance_uid}/instances/${sop_instance_uid}/frames/${frame_number}`, true);
    xhr.setRequestHeader('Accept', 'multipart/related; type=image/dicom+jpeg; transfer-syntax=1.2.840.10008.1.2.4.70');
    xhr.responseType = 'arraybuffer';

    xhr.onerror = (ev) => {
      self.onerror_callback(new ERROR('CONNECTION', ev.message));
    }

    xhr.onload = function () {
      if (xhr.status != 200) {
        self.onerror_callback(new ERROR('SERVER', xhr.statusText, xhr.status));
        return;
      }

      let content_type_regex = new RegExp('boundary=(\\w+)', 'g');
      let match = content_type_regex.exec(xhr.getResponseHeader('content-type'));
      if (!match || match.length != 2) {
        self.onerror_callback(new ERROR('CLIENT', 'Could not match boundary from server: Content-Type: ' + xhr.getResponseHeader('content-type'), 1));
        return;
      }

      let arrayBufferView = new Uint8Array(xhr.response);

      //let start_pattern = self.encoder.encode('--' + match[1]);
      let start_pattern = self.encoder.encode('\r\n\r\n');

      let i = KMPAlgorithmImpl.kmpSearch(WADOClient.START_CONTENT_PATTERN.pattern, arrayBufferView) + 4;
      let z = KMPAlgorithmImpl.kmpSearch(WADOClient.END_PATTERN.pattern, arrayBufferView.subarray(i), WADOClient.END_PATTERN.lsp);

      let arr = arrayBufferView.subarray(i, z);
      //console.log(arr.length);
      //console.log(arr);

      let objectURL = window.URL.createObjectURL(new Blob([arr], { type: 'image/jpeg' }));
      callback(objectURL);
    }

    xhr.send();
  }

  public requestFrames(study_instance_uid: string, series_instance_uid: string, sop_instance_uid: string, frame_number: number[], callback: MultiFrameSuccessCallback){
    let self = this;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', `${this.url}studies/${study_instance_uid}/series/${series_instance_uid}/instances/${sop_instance_uid}/frames/${frame_number.join(',')}`, true);
    xhr.setRequestHeader('Accept', 'multipart/related; type=image/dicom+jpeg; transfer-syntax=1.2.840.10008.1.2.4.70');
    xhr.responseType = 'arraybuffer';

    xhr.onerror = (ev) => {
      self.onerror_callback(new ERROR('CONNECTION', ev.message));
    }

    xhr.onload = function () {
      if (xhr.status != 200) {
        self.onerror_callback(new ERROR('SERVER', xhr.statusText, xhr.status));
        return;
      }

      let content_type_regex = new RegExp('boundary=(\\w+)', 'g');
      let match = content_type_regex.exec(xhr.getResponseHeader('content-type'));
      if (!match || match.length != 2) {
        self.onerror_callback(new ERROR('CLIENT', 'Could not match boundary from server: Content-Type: ' + xhr.getResponseHeader('content-type'), 1));
        return;
      }

      let arrayBufferView = new Uint8Array(xhr.response);
      
      let start_frame = self.encoder.encode('--' + match[1]);
      let start_frame_lsp = KMPAlgorithmImpl.kmpTable(start_frame);
      let end_frame = self.encoder.encode('\r\n--' + match[1]);
      let end_frame_lsp = KMPAlgorithmImpl.kmpTable(end_frame);
      
      let content_ended = false;
      let response_contents:Array<Uint8Array> = [];
      let max_iter = 0; // Safetty Call.
      
      let current_pos:number = KMPAlgorithmImpl.kmpSearch(start_frame, arrayBufferView, start_frame_lsp) + start_frame.length;
      let end_pos = arrayBufferView.length;

      while (max_iter < frame_number.length && !content_ended) {
        let content_init_index = KMPAlgorithmImpl.kmpSearchInSubArray(WADOClient.START_CONTENT_PATTERN.pattern, arrayBufferView, current_pos, end_pos, WADOClient.START_CONTENT_PATTERN.lsp) + 4;
        let content_end_index = KMPAlgorithmImpl.kmpSearchInSubArray(end_frame, arrayBufferView, content_init_index,end_pos, end_frame_lsp);
        //console.log(`Start: ${current_pos} : ${content_init_index} : ${content_end_index}`);

        if(content_init_index == -1 || content_end_index == -1){
          //Error Could not match line feeds.
          self.onerror_callback(new ERROR('CLIENT', 'Could not find content boundaries in the server response.', 2));
          return;
        }

        response_contents.push(arrayBufferView.subarray(content_init_index, content_end_index));
        
        content_end_index += end_frame.length;
        current_pos = content_end_index;

        content_ended = arrayBufferView[current_pos] == 45 && arrayBufferView[current_pos+1] == 45
        max_iter++;
      }

      let object_url_list = [];
      for(let i in response_contents){
        let content_arr = response_contents[i];
        //console.log(content_arr);
        let objectURL = window.URL.createObjectURL(new Blob([content_arr], { type: 'image/jpeg' }));      
        object_url_list.push(objectURL);
      }
      callback(object_url_list);
    }

    xhr.send();
  }
}

export class ERROR {
  readonly type: any;
  readonly code: number;
  readonly message: string;

  constructor(type: any, message: string, code?: number) {
    this.type = type;
    this.code = code;
    this.message = message;
  }


}

// 404 Not Found Specified resource does not exist
